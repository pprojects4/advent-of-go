package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"slices"
	"strconv"
	"strings"
)

func main() {
    playBingo("./input.txt")
}

type MarkedNumber struct {
    row int
    col int
}

type Board struct {
    rowOne   []int
    rowTwo   []int
    rowThree []int
    rowFour  []int
    rowFive  []int
    marked   []MarkedNumber
    bingo    bool
}

func newBoard() Board {
    return Board{
        rowOne: make([]int, 0),
        rowTwo: make([]int, 0),
        rowThree: make([]int, 0),
        rowFour: make([]int, 0),
        rowFive: make([]int, 0),
        marked: make([]MarkedNumber, 0),
        bingo: false,
    }
}

func (b *Board) lookup(row int, col int) int {
    if row == 0 {
        return b.rowOne[col]
    } else if row == 1 {
        return b.rowTwo[col]
    } else if row == 2 {
        return b.rowThree[col]
    } else if row == 3 {
        return b.rowFour[col]
    } else if row == 4 {
        return b.rowFive[col]
    }
    log.Fatal("Invalid board lookup")
    return -1
}

func (b *Board) addNext(nums []int) *Board {
    if len(b.rowOne) == 0 {
        b.rowOne = nums
    } else if len(b.rowTwo) == 0 {
        b.rowTwo = nums
    } else if len(b.rowThree) == 0 {
        b.rowThree = nums
    } else if len(b.rowFour) == 0 {
        b.rowFour = nums
    } else if len(b.rowFive) == 0 {
        b.rowFive = nums
    } else {
        log.Fatal("Board is not empty")
    }
    return b
}

func (b Board) checkWin(num int) (Board, bool) {
    for index, row := range [][]int{b.rowOne, b.rowTwo, b.rowThree, b.rowFour, b.rowFive} {
        i := slices.Index(row, num)
        if i != -1 {
            markedNum := MarkedNumber{row: index, col: i}
            b.marked = append(b.marked, markedNum)
            rowWin := true
            colWin := true
            for num_i := range row {
                if !slices.Contains(b.marked, MarkedNumber{row: index, col: num_i}) {
                    rowWin = false
                    break
                }
            }
            for num_i := range []int{b.rowOne[i], b.rowTwo[i], b.rowThree[i], b.rowFour[i], b.rowFive[i]} {
                if !slices.Contains(b.marked, MarkedNumber{row: num_i, col: i}) {
                    colWin = false
                    break
                }
            }
            return b, rowWin || colWin
        }
    }
    return b, false
}

func (b *Board) calculate(calledNum int) int {
    sum := 0
    for rowIndex, row := range [][]int{b.rowOne, b.rowTwo, b.rowThree, b.rowFour, b.rowFive} {
        for colIndex, num := range row {
            if !slices.Contains(b.marked, MarkedNumber{row: rowIndex, col: colIndex}) {
                sum += num
            }
        }
    }
    return sum * calledNum
}


func playBingo(file_path string) {
    file, err := os.Open(file_path)
    if err != nil {
        log.Fatal("Unable to open file")
    }
    defer file.Close()

    var nums []int
    current_row := 1
    board := newBoard()
    var boards []Board
    scanner := bufio.NewScanner(file)
    for scanner.Scan()  {
        val := scanner.Text()
        if len(nums) == 0 {
            for _, numStr := range strings.Split(val, ",") {
                num, err := strconv.Atoi(numStr)
                if err != nil {
                    log.Fatal("Unable to parse number")
                }
                nums = append(nums, num)
            }
            continue
        }
        if val == "" {
            continue
        }
        boardNums := make([]int, 0)
        for _, numStr := range strings.Split(val, " ") {
            if numStr == "" {
                continue
            }
            num, err := strconv.Atoi(numStr)
            if err != nil {
                log.Fatal("Unable to parse board number:", numStr)
            }
            boardNums = append(boardNums, num)
        }
        board.addNext(boardNums)
        if current_row == 5 {
            boards = append(boards, board)
            board = newBoard()
            current_row = 0
        }
        current_row += 1
    }
    var wins []Board
    firstFound := false

    for _, num := range nums {
        for i, board := range boards {
            if board.bingo {
                continue
            }
            board, isWin := board.checkWin(num)
            boards[i] = board
            if isWin {
                board.bingo = true
                boards[i] = board
                if !firstFound {
                    fmt.Println("WIN", board.calculate(num))
                    firstFound = true
                }
                wins = append(wins, board)
            }
        }
    }
    last := wins[len(wins) - 1]
    lastMarked := last.marked[len(last.marked) - 1]
    num := last.lookup(lastMarked.row, lastMarked.col)
    fmt.Println("Last Win", last.calculate(num))
}
