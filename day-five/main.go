package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
    partOne("./input.txt")
}

type Position struct {
    col int
    row int
}

func newPosition(part string) Position {
    parts := strings.Split(part, ",")
    col, colErr := strconv.Atoi(parts[0])
    row, rowErr := strconv.Atoi(parts[1])
    if colErr != nil || rowErr != nil {
        log.Fatal("Unable to parse number")
    }
    return Position { col, row }
}

func partOne(filePath string) {
    file, err := os.Open(filePath)
    if err != nil {
        log.Fatal("Unable to open file")
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan()  {
        line := scanner.Text()
        parts := strings.Split(line, " -> ")
        start := newPosition(parts[0])
        end := newPosition(parts[1])
        
        
        
        fmt.Println(start, " <> ", end)
    }
    
}