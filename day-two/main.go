package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
    part_one()
    part_two()
}

type Commands int

const (
    Up = iota
    Forward
    Down
)

var commandName = map[Commands]string{
    Up: "up",
    Forward: "forward",
    Down: "down",
}

func (c Commands) String() string {
    return commandName[c]
}

type Direction struct {
    command string
    number int
}

func newDirection(line string) (Direction, error) {
    parts := strings.Split(line, " ")
    if len(parts) != 2 {
        return Direction{"Up", 0}, errors.New("Invalid command line")
    }
    cmd, numStr := parts[0], parts[1]
    num, err := strconv.Atoi(numStr)
    if err != nil {
        return Direction{cmd, 0}, errors.New("Invalid command line")
    }
    return Direction{cmd, num}, nil
}

func part_one() {
    file, err := os.Open("./input.txt")
    if err != nil {
        log.Fatal("Unable to read file")
    }
    defer file.Close()

    depth := 0
    horizontal := 0
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        val := scanner.Text()
        direction, err := newDirection(val)
        if err != nil {
            log.Fatal("Unable to parse line")
        }
        switch direction.command {
            case commandName[Forward]:
                horizontal += direction.number
            case commandName[Up]:
                depth -= direction.number
            case commandName[Down]:
                depth += direction.number       
        }
    }
    fmt.Println("Part One:", depth * horizontal)
}

func part_two() {
    file, err := os.Open("./input.txt")
    if err != nil {
        log.Fatal("Unable to read file")
    }
    defer file.Close()

    aim := 0
    depth := 0
    horizontal := 0
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        val := scanner.Text()
        direction, err := newDirection(val)
        if err != nil {
            log.Fatal("Unable to parse line")
        }
        switch direction.command {
            case commandName[Forward]:
                horizontal += direction.number
                depth += direction.number * aim
            case commandName[Up]:
                aim -= direction.number
            case commandName[Down]:
                aim += direction.number
        }
    }
    fmt.Println("Part One:", depth * horizontal)
}
