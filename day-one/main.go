package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
    count_increases()
    count_ranges()
}

func count_increases() {
    file, err := os.Open("./input.txt")
    if err != nil {
        log.Fatal("Unable to read file")
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    count := 0
    var prev int;
    for scanner.Scan() {
        val, err := strconv.Atoi(scanner.Text())
        if err != nil {
            panic("Failed to parse value")
        }
        if prev == 0 {
            prev = val
            continue
        }
        if val > prev {
            count += 1
        }
        prev = val
    }
    fmt.Println("Total:", count)
}

func count_ranges() {
    file, err := os.Open("./input.txt")
    if err != nil {
        log.Fatal("Unable to read file")
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    var elements []int

    for scanner.Scan() {
        val, err := strconv.Atoi(scanner.Text())
        if err != nil {
            panic("Failed to parse value")
        }
        elements = append(elements, val)
    }

    count := 0
    var prev int
    for i, val := range elements {
        if i < 2 {
            continue
        }
        total := elements[i - 2] + elements[i - 1] + val
        if prev == 0 {
            prev = total
            continue
        }
        if total > prev {
            count += 1
        }
        prev = total
    }
    fmt.Println("Part Two:", count)
}
