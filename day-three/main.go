package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
    part_one("./input.txt")
}

type Position struct {
    zeros int
    ones  int
}

func (p *Position) Add(bit string) {
    if bit == "0" {
        p.zeros += 1
    } else if bit == "1" {
        p.ones += 1
    }
}

func newPosition(bit string) Position {
    position := Position{
        zeros: 0,
        ones: 0,
    }
    position.Add(bit)
    return position
}

func part_one(file_path string) {
    file, err := os.Open(file_path)
    if err != nil {
        log.Fatal("Unable to read file")
    }
    defer file.Close()

    gamma := ""
    epsilon := ""
    counts := make(map[int]Position)
    scanner := bufio.NewScanner(file)
    var lines []string
    line_len := 0
    for scanner.Scan() {
        var val string = scanner.Text()
        lines = append(lines, val)
        for i, c := range val {
            bit := string(c)
            position, found := counts[i]
            if !found {
                counts[i] = newPosition(bit)
            } else {
                position.Add(bit)
                counts[i] = position
            }
        }
        if line_len == 0 {
            line_len = len(val)
        }
    }
    for i := 0; i < line_len; i++ {
        value := counts[i]
        if value.zeros > value.ones {
            gamma += "0"
            epsilon += "1"
        } else {
            gamma += "1"
            epsilon += "0"
        }
        
    }
    gamma_total, _ := strconv.ParseInt(gamma, 2, 32)
    epsilon_total, _ := strconv.ParseInt(epsilon, 2, 32)
    fmt.Println("Total:", gamma_total * epsilon_total)

    oxygen := findRating(lines, 0, true)
    co2 := findRating(lines, 0, false)
    fmt.Println("Part Two", oxygen * co2)
}

func findRating(ratings []string, bit_index int, max bool) int64 {
    if len(ratings) == 1 {
        num, err := strconv.ParseInt(ratings[0], 2, 64)
        if err != nil {
            log.Fatal("Unable to parse number")
        }
        fmt.Println("Found ", num, ratings, max)
        return num
    }
    if bit_index >= len(ratings[0]) {
        log.Fatal("Index too large")
    }
    var zeros []string
    var ones []string
    for i := 0; i < len(ratings); i++ {
        rating := ratings[i]
        bit := string(rating[bit_index])
        if bit == "0" {
            zeros = append(zeros, ratings[i])
        } else {
            ones = append(ones, rating)
        }
    }
    if max {
        if len(zeros) > len(ones) {
            return findRating(zeros, bit_index + 1, max)
        } else {
            return findRating(ones, bit_index + 1, max)
        }
    } else {
        if len(zeros) > len(ones) {
            return findRating(ones, bit_index + 1, max)
        } else {
            return findRating(zeros, bit_index + 1, max)
        }
    }
}
